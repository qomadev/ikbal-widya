import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoverPage extends StatefulWidget {
  const CoverPage({Key? key}) : super(key: key);

  @override
  State<CoverPage> createState() => _CoverPageState();
}

String? linkUrl;

class _CoverPageState extends State<CoverPage> {
  String? sendTo;

  @override
  void initState() {
    super.initState();

    var encoded = Uri.parse(Uri.base.toString());
    linkUrl = encoded.toString();
    sendTo = encoded.query.replaceAll("to=", "").replaceAll("+", " ");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Image.asset('assets/images/cover_page/img_cover.jpeg',
                    fit: BoxFit.cover)),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                    alignment: Alignment.topCenter,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 38),
                          child: Text('The Wedding Of',
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 14,
                                letterSpacing: 5,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              )),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 13),
                          width: 206,
                          height: 83,
                          child: Image.asset(
                              "assets/images/cover_page/text_black.png"),
                        ),
                      ],
                    )),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 150),
                      child: Text(
                        'Kepada Yth/ Bapak/ Ibu/ Saudara/ i :',
                        style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Color.fromRGBO(255, 255, 255, 1)),
                      ),
                    ),
                    Text(
                      "$sendTo",
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 18,
                          fontWeight: FontWeight.w800,
                          color: Color.fromRGBO(255, 255, 255, 1)),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(top: 13),
                  height: 42,
                  width: 125,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color.fromRGBO(202, 164, 68, 1)),
                  child: const Text(
                    'Buka Undangan',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
