// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, duplicate_ignore, prefer_const_literals_to_create_immutables

import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:ikbal_widya/cover_page.dart';
import 'package:ikbal_widya/landing_page/closing_page.dart';
import 'package:ikbal_widya/landing_page/gallery_page.dart';
import 'package:ikbal_widya/landing_page/protocol_page.dart';
import 'package:ikbal_widya/landing_page/wedding_page.dart';
import 'package:ikbal_widya/landing_page/who_page.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  AudioCache audioCache = AudioCache();
  AudioPlayer audioPlayer = AudioPlayer();
  PlayerState audioPlayerState = PlayerState.PAUSED;

  bool _isTapped = false;

  @override
  void initState() {
    super.initState();

    audioCache = AudioCache(fixedPlayer: audioPlayer);
    audioPlayer.onPlayerStateChanged.listen((PlayerState s) {
      setState(() {
        audioPlayerState = s;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    audioPlayer.release();
    audioPlayer.dispose();
    audioCache.clearAll();
  }

  playLoopedMusic() async {
    await audioCache.loop('music/music_kecapi.mp3');
  }

  pauseMusic() async {
    await audioPlayer.pause();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Stack(
            children: [
              ScrollConfiguration(
                behavior: ScrollBehavior(),
                child: GlowingOverscrollIndicator(
                  color: Color.fromRGBO(202, 164, 68, 1),
                  axisDirection: AxisDirection.down,
                  child: Container(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    child: ListView(
                      children: [
                        WeddingPage(),
                        WhoPage(),
                        GalleryPage(),
                        ProtocolPage(),
                        ClosingPage()
                      ],
                    ),
                  ),
                ),
              ),
              AnimatedPositioned(
                duration: Duration(milliseconds: 550),
                top: (_isTapped) ? -MediaQuery.of(context).size.height : 0,
                bottom: (_isTapped) ? MediaQuery.of(context).size.height : 0,
                left: 0,
                right: 0,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      _isTapped = true;
                      audioPlayerState == PlayerState.PLAYING
                          ? pauseMusic()
                          : playLoopedMusic();
                    });
                  },
                  child: CoverPage(),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: SizedBox(
          height: 30,
          width: 30,
          child: FloatingActionButton(
            backgroundColor: Color.fromRGBO(202, 164, 68, 1).withOpacity(0.5),
            onPressed: () async {
              audioPlayerState == PlayerState.PLAYING
                  ? pauseMusic()
                  : playLoopedMusic();
            },
            child: Icon(audioPlayerState == PlayerState.PLAYING
                ? Icons.music_note
                : Icons.music_off),
          ),
        ),
      ),
    );
  }
}
