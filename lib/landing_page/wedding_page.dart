import 'dart:async';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:date_count_down/countdown.dart';

class WeddingPage extends StatefulWidget {
  const WeddingPage({Key? key}) : super(key: key);

  @override
  _WeddingPageState createState() => _WeddingPageState();
}

class _WeddingPageState extends State<WeddingPage> {
  String countTime = 'Loading...';
  Timer? _timer;
  List<String> pecahanCT = [];

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(
      const Duration(seconds: 1),
      (timer) {
        setState(() {});
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _timer!.cancel();
  }

  @override
  Widget build(BuildContext context) {
    countTime = CountDown().timeLeft(DateTime.parse("2021-12-12"), "Completed");
    pecahanCT = (countTime == "Completed") ? [] : countTime.split(" ");
    return Column(
      children: [
        Stack(
          children: [
            Image.asset('assets/images/wedding_page/img_wedding.jpeg'),
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 35),
                    child: RichText(
                        text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'Ikbal',
                            style: TextStyle(
                                fontFamily: 'Milonga',
                                fontSize: 32,
                                letterSpacing: 3,
                                color: Colors.white)),
                        TextSpan(
                            text: ' & ',
                            style: TextStyle(
                                fontFamily: 'Milonga',
                                fontSize: 20,
                                color: Color.fromRGBO(202, 164, 68, 1))),
                        TextSpan(
                            text: 'Widya',
                            style: TextStyle(
                                fontFamily: 'Milonga',
                                fontSize: 32,
                                letterSpacing: 3,
                                color: Colors.white)),
                      ],
                    )),
                  ),
                  Column(
                    children: [
                      Text(
                        'Kami akan menikah,',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 10,
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        'dan kami ingin kamu menjadi bagian di hari spesial kami !',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 10,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 9),
                      Container(
                        padding: const EdgeInsets.only(bottom: 5),
                        width: 154,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(15, 15, 15, 0.8),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Text(
                                    (pecahanCT.isEmpty)
                                        ? "00"
                                        : (pecahanCT[0].length < 2)
                                            ? "0${pecahanCT[0]}"
                                            : "${pecahanCT[0]}",
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        color:
                                            Color.fromRGBO(202, 164, 68, 1))),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text('Hari',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 10,
                                      color: Color.fromRGBO(202, 164, 68, 1),
                                    )),
                              ],
                            ),
                            SizedBox(width: 10),
                            Column(
                              children: [
                                Text(
                                    (pecahanCT.isEmpty)
                                        ? "00"
                                        : (pecahanCT[2].length < 2)
                                            ? "0${pecahanCT[2]}"
                                            : "${pecahanCT[2]}",
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        color:
                                            Color.fromRGBO(202, 164, 68, 1))),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text('Jam',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 10,
                                      color: Color.fromRGBO(202, 164, 68, 1),
                                    )),
                              ],
                            ),
                            SizedBox(width: 10),
                            Column(
                              children: [
                                Text(
                                    (pecahanCT.isEmpty)
                                        ? "00"
                                        : (pecahanCT[4].length < 2)
                                            ? "0${pecahanCT[4]}"
                                            : "${pecahanCT[4]}",
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        color:
                                            Color.fromRGBO(202, 164, 68, 1))),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text('Menit',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 10,
                                      color: Color.fromRGBO(202, 164, 68, 1),
                                    )),
                              ],
                            ),
                            SizedBox(width: 10),
                            Column(
                              children: [
                                Text(
                                    (pecahanCT.isEmpty)
                                        ? "00"
                                        : "${pecahanCT[6]}",
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        color:
                                            Color.fromRGBO(202, 164, 68, 1))),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text('Detik',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 10,
                                      color: Color.fromRGBO(202, 164, 68, 1),
                                    )),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        Column(
          children: [
            Stack(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 15),
                  child: Center(
                    child: const Text(
                      '#IkbaldanWidya',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 8,
                          letterSpacing: 2,
                          color: Color.fromRGBO(0, 0, 0, 1)),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
              child: Center(
                child: Text(
                  'Minggu',
                  style: const TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 20,
                      letterSpacing: 3,
                      color: Color.fromRGBO(202, 164, 68, 1)),
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
              child: Center(
                child: Text(
                  "Desember 12, 2021",
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 24,
                      color: Color.fromRGBO(0, 0, 0, 1)),
                ),
              ),
            ),
            SizedBox(height: 8),
            Container(
              child: Center(
                child: Text('Gedung Amanah Al-Barokah',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 20,
                        color: Color.fromRGBO(0, 0, 0, 1))),
              ),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      await launch(
                          'https://calendar.google.com/event?action=TEMPLATE&tmeid=MWRnaWswa29lcGlqc21pcmhzb2lpZmt1OTkgcW9tYS5kZXZAbQ&tmsrc=qoma.dev%40gmail.com');
                    },
                    child: Container(
                      height: 42,
                      color: Color.fromRGBO(15, 15, 15, 0.8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 6),
                            width: 16,
                            height: 16,
                            child: Image.asset('assets/images/icon/date.png'),
                          ),
                          Text('Simpan Tanggal',
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 10,
                                color: Color.fromRGBO(202, 164, 68, 1),
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 6),
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      if (await canLaunch(
                          "https://www.google.co.id/maps/place/Masjid+Jami+Amanah+Al-Barokah/@-7.2042275,107.590693,21z/data=!4m8!1m2!2m1!1sgedung+serba+guna+amanah+pangalengan!3m4!1s0x2e6897a715fcfd77:0x163f62a8f6f84166!8m2!3d-7.2042858!4d107.5906232")) {
                        await launch(
                            "https://www.google.co.id/maps/place/Masjid+Jami+Amanah+Al-Barokah/@-7.2042275,107.590693,21z/data=!4m8!1m2!2m1!1sgedung+serba+guna+amanah+pangalengan!3m4!1s0x2e6897a715fcfd77:0x163f62a8f6f84166!8m2!3d-7.2042858!4d107.5906232");
                      } else {
                        throw 'Could not Launch Maps';
                      }
                    },
                    child: Container(
                      height: 42,
                      color: Color.fromRGBO(15, 15, 15, 0.8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 6),
                            width: 16,
                            height: 16,
                            child:
                                Image.asset('assets/images/icon/location.png'),
                          ),
                          Text('Lihat Lokasi',
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 10,
                                color: Color.fromRGBO(202, 164, 68, 1),
                              )),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
        SizedBox(height: 30),
        Text(
          'Acara Pernikahan Kami',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Milonga',
            fontSize: 40,
            letterSpacing: 2,
            color: Color.fromRGBO(202, 164, 68, 1),
          ),
        ),
        SizedBox(height: 10),
        Container(
          height: 200,
          width: 300,
          padding: EdgeInsets.only(top: 32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Color.fromRGBO(15, 15, 15, 0.8),
          ),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(right: 70),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/icon/img_akad.png',
                      height: 46.13,
                      width: 46.2,
                    ),
                    SizedBox(width: 15),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'AKAD',
                          style: TextStyle(
                            fontFamily: 'Averia',
                            fontSize: 24,
                            // letterSpacing: 6,
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(202, 164, 68, 1),
                          ),
                        ),
                        SizedBox(height: 1),
                        Text(
                          '08.00',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/icon/img_resepsi.png',
                    height: 46.13,
                    width: 49.5,
                  ),
                  SizedBox(width: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'RESEPSI',
                        style: TextStyle(
                          fontFamily: 'Averia',
                          fontSize: 24,
                          // letterSpacing: 6,
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(202, 164, 68, 1),
                        ),
                      ),
                      SizedBox(height: 1),
                      Text(
                        '11.00 s/d Selesai',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(255, 255, 255, 1),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: 30),
      ],
    );
  }
}
