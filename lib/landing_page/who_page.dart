// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, sized_box_for_whitespace

import 'package:flutter/material.dart';

class WhoPage extends StatelessWidget {
  const WhoPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(25),
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Column(
        children: [
          Container(
            child: Column(
              children: [
                Image.asset('assets/images/who_page/img_cewe.png'),
                Container(
                  color: Color.fromRGBO(15, 15, 15, 0.8),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      SizedBox(height: 14),
                      Text('Mempelai Wanita',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 10,
                            letterSpacing: 3,
                            color: Color.fromRGBO(202, 164, 68, 1),
                          )),
                      SizedBox(height: 5),
                      Text('Salsabila Widia Agisti',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: 'Milonga',
                              fontSize: 36,
                              color: Colors.white)),
                      SizedBox(height: 7),
                      Container(
                        height: 1,
                        width: 76,
                        color: Color.fromRGBO(255, 238, 235, 1),
                      ),
                      SizedBox(height: 8),
                      Text('anak dari',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 10,
                            color: Colors.white,
                          )),
                      SizedBox(height: 2),
                      Text('Bapak Imam Musrotul Fauzi & Ibu Elis Saida Widia',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 10,
                            color: Colors.white,
                          )),
                      SizedBox(height: 15),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 23),
          Container(
            color: Color.fromRGBO(255, 255, 255, 1),
            child: Column(
              children: [
                Container(
                  child: Column(
                    children: [
                      Image.asset('assets/images/who_page/img_cowo.png'),
                      Container(
                        color: Color.fromRGBO(15, 15, 15, 0.8),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: [
                            SizedBox(height: 14),
                            Text('Mempelai Pria',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 10,
                                  letterSpacing: 3,
                                  color: Color.fromRGBO(202, 164, 68, 1),
                                )),
                            SizedBox(height: 5),
                            Text('Ikbal Maulana Yusuf',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: 'Milonga',
                                    fontSize: 36,
                                    color: Colors.white)),
                            SizedBox(height: 7),
                            Container(
                              height: 1,
                              width: 76,
                              color: Colors.white,
                            ),
                            SizedBox(height: 8),
                            Text('anak dari',
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 10,
                                  color: Colors.white,
                                )),
                            SizedBox(height: 2),
                            Text('Bapak Asep Supriatna & Ibu Nurleala Sari',
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 10,
                                  color: Colors.white,
                                )),
                            SizedBox(height: 15),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
