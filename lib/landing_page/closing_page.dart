// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers, sized_box_for_whitespace

import 'package:flutter/material.dart';

class ClosingPage extends StatelessWidget {
  const ClosingPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Column(
        children: [
          Image.asset('assets/images/closing_page/img_closing.jpeg'),
          Container(
            child: Column(
              children: [
                SizedBox(height: 21),
                Container(
                  width: 20,
                  height: 20,
                  child: Image.asset('assets/images/icon/kutip.png'),
                ),
                SizedBox(height: 14),
                Center(
                  child: Text(
                    'Dan di antara tanda -tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.italic,
                        letterSpacing: 1),
                  ),
                ),
                SizedBox(height: 5),
                Center(
                  child: Text(
                    'Ar-Rum : 21',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 8,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w600,
                        color: Color.fromRGBO(202, 164, 68, 1)),
                  ),
                ),
                SizedBox(height: 21),
                Text(
                  'With Love',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 10,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 2,
                    color: Color.fromRGBO(202, 164, 68, 1),
                  ),
                ),
                RichText(
                    text: TextSpan(
                  children: [
                    TextSpan(
                        text: 'Ikbal',
                        style: TextStyle(
                            fontFamily: 'Milonga',
                            fontSize: 36,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 3,
                            color: Color.fromRGBO(202, 164, 68, 1))),
                    TextSpan(
                        text: ' & ',
                        style: TextStyle(
                            fontFamily: 'Milonga',
                            fontSize: 22,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(15, 15, 15, 0.8))),
                    TextSpan(
                        text: 'Widya',
                        style: TextStyle(
                            fontFamily: 'Milonga',
                            fontSize: 36,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 3,
                            color: Color.fromRGBO(202, 164, 68, 1))),
                  ],
                )),
                SizedBox(height: 8),
                Container(
                    height: 1,
                    width: 90,
                    color: Color.fromRGBO(255, 255, 255, 1)),
                SizedBox(height: 27),
                Text(
                  'Design by',
                  style: TextStyle(
                      fontFamily: 'Averia',
                      fontSize: 5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2,
                      color: Color.fromRGBO(15, 15, 15, 0.8)),
                ),
                SizedBox(height: 4),
                Text(
                  'QOMA',
                  style: TextStyle(
                      fontFamily: 'Averia',
                      fontSize: 8,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 3,
                      color: Color.fromRGBO(15, 15, 15, 0.8)),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
