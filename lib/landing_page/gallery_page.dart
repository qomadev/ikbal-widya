import 'package:flutter/material.dart';

class GalleryPage extends StatefulWidget {
  const GalleryPage({ Key? key }) : super(key: key);

  @override
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  List<String> img = [
        'assets/images/gallery_page/gal1.jpeg'
        'assets/images/gallery_page/gal2.jpeg'
        'assets/images/gallery_page/gal3.jpeg'
        'assets/images/gallery_page/gal4.jpeg'
        'assets/images/gallery_page/gal5.jpeg'
        'assets/images/gallery_page/gal6.jpeg'
        'assets/images/gallery_page/gal7.jpeg'
        'assets/images/gallery_page/gal8.jpeg'
  ];
  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          color: Color.fromRGBO(255, 255, 255, 1),
          child: Column(
            children: [
              SizedBox(height: 14),
              Text('ALBUM KAMI',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 22,
                    letterSpacing: 3,
                    color: Color.fromRGBO(202, 164, 68, 1),
                  )),
              SizedBox(height: 17),
              Container(
                  color: Color.fromRGBO(15, 15, 15, 0.8),
                  height: 193,
                  width: MediaQuery.of(context).size.width,
                  child: ScrollConfiguration(
                    behavior: ScrollBehavior(),
                    child: GlowingOverscrollIndicator(
                      color: Color.fromRGBO(202, 164, 68, 1),
                      axisDirection: AxisDirection.right,
                      child: ListView.builder(
                        padding: EdgeInsets.fromLTRB(20, 7, 10, 7),
                        scrollDirection: Axis.horizontal,
                        itemCount: 8,
                        itemBuilder: (context, index) => Row(children: [
                          GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) => Dialog(
                                          child: AspectRatio(
                                        aspectRatio: 1 / 1,
                                        child: Container(
                                          color:
                                              Color.fromRGBO(15, 15, 15, 0.8),
                                          child: Image.asset(
                                            'assets/images/gallery_page/gal${index + 1}.jpeg',
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      )));
                            },
                            child: Image.asset(
                              'assets/images/gallery_page/gal${index + 1}.jpeg',
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(width: 10)
                        ]),
                      ),
                    ),
                  )),
            ],
          ),
        ),
      ],
    );
  }
}