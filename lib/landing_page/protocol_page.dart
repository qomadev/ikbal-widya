// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class ProtocolPage extends StatelessWidget {
  const ProtocolPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(8),
            color: Color.fromRGBO(15, 15, 15, 0.8),
            child: Center(
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text('PROTOKOL KESEHATAN',
                        style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 16,
                            letterSpacing: 5,
                            fontWeight: FontWeight.w800,
                            color: Color.fromRGBO(202, 164, 68, 1))),
                  ),
                  SizedBox(height: 2.12),
                  Text(
                    'Untuk menjaga keberlangsungan acara ini agar aman dari resiko penularan covid-19, mohon simak anjuran berikut sebelum anda hadir ke lokasi',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      letterSpacing: 1,
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(255, 255, 255, 1),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 20),
          GridView.count(
            physics: NeverScrollableScrollPhysics(),
            crossAxisCount: 2,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            shrinkWrap: true,
            children: [
              Container(
                color: Color.fromRGBO(15, 15, 15, 0.8),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    SizedBox(height: 10.6),
                    Text('Memakai Masker',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Proza',
                          fontSize: 12,
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        )),
                    SizedBox(height: 5),
                    Expanded(child: Image.asset('assets/images/icon/masker.png')),
                    SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 10, right: 10, left: 10),
                      child: Align(
                          alignment: Alignment.center,
                          child: Text(
                              'Menggunakan masker ketika acara berlangsung',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Proza',
                                  letterSpacing: 1,
                                  fontSize: 10,
                                  color: Color.fromRGBO(255, 255, 255, 1)))),
                    )
                  ],
                ),
              ),
              Container(
                color: Color.fromRGBO(15, 15, 15, 0.8),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    SizedBox(height: 10.6),
                    Text('Menjaga Jarak',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Proza',
                          fontSize: 12,
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        )),
                    SizedBox(height: 5),
                    Expanded(
                        child: Image.asset('assets/images/icon/jaga_jarak.png')),
                    SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 10, right: 10, left: 10),
                      child: Align(
                          alignment: Alignment.center,
                          child: Text('Menjaga jarak antar tamu yang hadir',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Proza',
                                  letterSpacing: 1,
                                  fontSize: 10,
                                  color: Color.fromRGBO(255, 255, 255, 1)))),
                    )
                  ],
                ),
              ),
              Container(
                color: Color.fromRGBO(15, 15, 15, 0.8),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    SizedBox(height: 10.6),
                    Text('Mencuci Tangan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Proza',
                          fontSize: 12,
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        )),
                    SizedBox(height: 5),
                    Expanded(
                        child: Image.asset('assets/images/icon/cuci_tangan.png')),
                    SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 10, right: 10, left: 10),
                      child: Align(
                          alignment: Alignment.center,
                          child: Text(
                              'Wajib mencuci tangan sebelum memasuki lokasi acara',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Proza',
                                  fontSize: 10,
                                  letterSpacing: 1,
                                  color: Color.fromRGBO(255, 255, 255, 1)))),
                    )
                  ],
                ),
              ),
              Container(
                color: Color.fromRGBO(15, 15, 15, 0.8),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    SizedBox(height: 10.6),
                    Text('Cek Suhu Badan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Proza',
                          fontSize: 12,
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        )),
                    SizedBox(height: 5),
                    Expanded(child: Image.asset('assets/images/icon/cek_suhu.png')),
                    SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 10, right: 10, left: 10),
                      child: Align(
                          alignment: Alignment.center,
                          child: Text(
                              'Tamu akan dicek suhu badan sebelum memasuki lokasi',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Proza',
                                  letterSpacing: 1,
                                  fontSize: 10,
                                  color: Color.fromRGBO(255, 255, 255, 1)))),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 19.92),
          Column(
            children: [
              Container(
                  padding: EdgeInsetsDirectional.all(10),
                  color: Color.fromRGBO(15, 15, 15, 0.8),
                  child: Center(
                    child: Column(
                      children: [
                        Text(
                            '“Semoga Allah menghimpun yang terserak dari keduanya, memberkati mereka berdua dan kiranya Allah meningkatkan kualitas keturunan mereka, menjaganya, membuka pintu rahmat, sumber ilmu dan hikmah serta memberi rasa aman bagi umat.”',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 12,
                              color: Color.fromRGBO(255, 255, 255, 1),
                            )),
                        SizedBox(height: 20),
                        Container(
                          padding: EdgeInsets.only(left: 5, right: 5),
                          child: Text(
                            'Doa Nabi Muhammad SAW, pada pernikahan Fatimah Azzahra dengan Ali bin Abi Thalib',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 10,
                                color: Color.fromRGBO(202, 164, 68, 1)),
                          ),
                        )
                      ],
                    ),
                  )),
            ],
          )
        ],
      ),
    );
  }
}
